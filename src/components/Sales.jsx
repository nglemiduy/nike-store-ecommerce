import React from 'react'
import Item from './utils/Item'
import Title from './utils/Title'

const Sales = ({ ifExists, endpoint: { title, items } }) => {
  // console.log(endpoint)
  return (
    <>
      <div className=' nike-container '>
        <Title title={title} />
        <div className={` grid items-center jsutify-items-center  gap-7 lg:gap-5 mt-7 ${ifExists ? 'grid-cols-3 xl:gird-cols-2 sm:grid-cols-1' : 'grid-col-4 xl:grid-cols-3 md:grid-cols-2 sm:grid-cols-1'} `}>
          {items?.map((item, i) => (
            <Item {...item} key={i} ifExists={ifExists} />
          ))}
          <Item />
        </div>
      </div>
    </>
  )
}

export default Sales